#! /usr/bin/env python
"""
Somewhat experimental script to check that the code matches the style used in
GitBuilding. This is a way of checkng things the linter cannot enforce, and
warning rather than outright failing a pipeline.
"""

import os
import re
import sys
from colorama import Fore, Style

def _get_all_py_files():
    all_py_files = []
    for root, dirs, files in os.walk('.'):
        poplist = []
        for n, directory in enumerate(dirs):
            if directory.startswith('.'):
                poplist.append(n)
        for n in poplist[::-1]:
            dirs.pop(n)
        for filename in files:
            if filename.endswith('.py'):
                all_py_files.append(os.path.join(root, filename))
    return all_py_files



def main():
    regexs = [r'^.*[\(\[\{]\s*$',
              r'^\s*[\)\]\}]\s*$']
    clean_exit = True
    for filepath in _get_all_py_files():
        with open(filepath, 'r', encoding='utf-8') as pyfile:
            code = pyfile.read()
        for regex in regexs:
            for bad_style in re.finditer(regex, code, re.MULTILINE):
                line_number = 1 + len(re.findall('\n', code[0:bad_style.start()]))
                print(Fore.RED
                      +f"\n{filepath} line {line_number}:\n{bad_style.group(0)}\nis not "
                      +"consistent with GitBuilding hanging indent style. See CONTRIBUTING.md"
                      +Style.RESET_ALL)
                clean_exit = False

    if clean_exit:
        print(Fore.GREEN+"\n\n This will pass on the CI!\n\n"+Style.RESET_ALL)
    else:
        print(Fore.RED+"\n\nThis will warn on the CI!\n\n"+Style.RESET_ALL)
        sys.exit(1)

if __name__ == "__main__":
    main()
