module.exports = {
  outputDir: "../../gitbuilding/static/webapp",
  publicPath: "/static/webapp/",
  pages: {
    buildup_editor: {
      entry: "src/buildup-editor-main.js",
      filename: "buildup-editor.html",
      template: "public/index.html"
    },
    conf_editor: {
      entry: "src/conf-editor-main.js",
      filename: "conf-editor.html",
      template: "public/index.html"
    },
    pdf_app: {
      entry: "src/pdf-app-main.js",
      filename: "pdf-app.html",
      template: "public/index.html"
    },
    contents_page: {
      entry: "src/contents-page-main.js",
      filename: "contents-page.html",
      template: "public/index.html"
    },
    new_page: {
      entry: "src/new-page-main.js",
      filename: "new-page.html",
      template: "public/index.html"
    },
    project_selector: {
      entry: "src/project-selector-main.js",
      filename: "project-selector.html",
      template: "public/index.html"
    }
  }
};
